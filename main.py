
from PyQt4.QtGui import QLabel, QWidget, QVBoxLayout,QHBoxLayout, QApplication, QTabWidget, QPushButton
import sys

from viewer import Viewer
import json

class MainWidget(QWidget):

    def __init__(self):
        super(MainWidget,self).__init__()
        self.labelWidget = QLabel("This is a first tab. There should be no viewer here")
        self.viewerWidget = QWidget()
        viewerLayout =QHBoxLayout()

        viewerLeftLayout = QVBoxLayout()
        viewerLeftLayout.addWidget(QLabel("Left part of widget. Viewer should be to the right of it"))

        self.projectChangedButton = QPushButton("Button to simulate project change (lower part should turn green and blue bound should dissapear)")
        self.projectChangedButton.clicked.connect(self.buttonPressed)
        viewerLeftLayout.addWidget(self.projectChangedButton)
        viewerLeftLayout.addStretch()

        viewerLayout.addLayout(viewerLeftLayout)

        with open('blue_bound.json') as pf:
            self.viewer = Viewer(json.load(pf))

        viewerLayout.addWidget(self.viewer)

        self.viewerWidget.setLayout(viewerLayout)

        self.tabs = QTabWidget()

        self.tabs.addTab(self.labelWidget,"No viewer")
        self.tabs.addTab(self.viewerWidget,"Viewer")

        mainLayout = QHBoxLayout()

        mainLayout.addWidget(self.tabs)

        self.setLayout(mainLayout)

    def buttonPressed(self):

        with open('green_halfcube.json') as pf:
            self.viewer.updateProjectState(json.load(pf))





if __name__ == '__main__':

    app = QApplication(sys.argv)

    mainWidget = MainWidget()
    mainWidget.show()

    sys.exit(app.exec_())