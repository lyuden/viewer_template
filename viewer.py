from PyQt4.QtGui import QWidget, QVBoxLayout, QHBoxLayout, QCheckBox, QLabel,QLineEdit
from PyQt4.QtOpenGL import QGLWidget

class ThreeDViewer(QGLWidget):
    def __init__(self, initial_gui_state, initial_project_state):
        super(ThreeDViewer,self).__init__()

        self.setGUIState(initial_gui_state)
        self.setProjectState(initial_project_state)

    def setGUIState(self,gui_state):
        print "Setting GUI State"

    def setProjectState(self, project_state):
        print "Setting Project State"

class CoordString(QHBoxLayout):
    def __init__(self,coord):
        super(CoordString,self).__init__()
        self.coord = coord
        self.label = QLabel(coord)
        self.lineEdit = QLineEdit('1.0')
        self.addWidget(self.label)
        self.addWidget(self.lineEdit)

    def getDict(self):
        return dict([(self.coord,float(self.lineEdit.text()))])

class Viewer(QWidget):
    def __init__(self, initial_project_state):
        super(Viewer,self).__init__()

        checkboxLayout = QVBoxLayout()


        self.meshCheckbox = QCheckBox("Enable Mesh")
        self.hatchingCheckbox = QCheckBox("Enable Haching")
        self.boundsCheckbox = QCheckBox("Show Bounds")

        self.meshCheckbox.stateChanged.connect(self.updateGUIState)
        self.hatchingCheckbox.stateChanged.connect(self.updateGUIState)
        self.boundsCheckbox.stateChanged.connect(self.updateGUIState)

        checkboxLayout.addWidget(self.meshCheckbox)
        checkboxLayout.addWidget(self.hatchingCheckbox)
        checkboxLayout.addWidget(self.boundsCheckbox)

        coordLayout = QVBoxLayout()

        self.x = CoordString('x')
        self.y = CoordString('y')
        self.z = CoordString('z')

        self.x.lineEdit.editingFinished.connect(self.updateGUIState)
        self.y.lineEdit.editingFinished.connect(self.updateGUIState)
        self.z.lineEdit.editingFinished.connect(self.updateGUIState)

        coordLayout.addLayout(self.x)
        coordLayout.addLayout(self.y)
        coordLayout.addLayout(self.z)

        topLayout = QHBoxLayout()

        topLayout.addLayout(checkboxLayout)
        topLayout.addLayout(coordLayout)

        mainLayout = QVBoxLayout()

        mainLayout.addLayout(topLayout)

        self.viewer3d = ThreeDViewer(self.getGUIState(),initial_project_state)

        mainLayout.addWidget(self.viewer3d)

        self.setLayout(mainLayout)


    def updateGUIState(self):
        self.viewer3d.setGUIState(self.getGUIState())

    def getGUIState(self):

        state = {}

        state.update(self.x.getDict())
        state.update(self.y.getDict())
        state.update(self.z.getDict())

        state['mesh'] = self.meshCheckbox.isChecked()
        state['hatching'] = self.hatchingCheckbox.isChecked()
        state['bounds'] = self.boundsCheckbox.isChecked()

        return state

    def updateProjectState(self,new_project_state):
        self.viewer3d.setProjectState(new_project_state)
